<?php

use App\Http\Controllers\Master\AgamaController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SpatieController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
    Route::get('/test', function () {
        return Inertia::render('Test');
    })->name('test');

});

Route::middleware(['auth:sanctum','verified',])->resource('users', UserController::class);



Route::group(['prefix' => 'master', 'middleware' => ['auth:sanctum','verified']], function(){
    Route::middleware(['auth:sanctum','verified',])->resource('agama', AgamaController::class);
    Route::middleware(['auth:sanctum','verified',])->resource('spatie', SpatieController::class);
    Route::middleware(['auth:sanctum','verified',])->resource('module', ModuleController::class);
});



Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::get('/test', [UserController::class, 'aksi'])->name('aksi');
});
