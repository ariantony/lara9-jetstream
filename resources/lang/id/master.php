<?php
return [
	"no" => "#",
    'id' => 'ID',
	"name" => "Nama",
	"description" => "Keterangan",
    "save" => "Simpan",
    "back" => "Kembali",
    "cancel" => "Batal",
	"profil" => [
		"name" => "Nama Lengkap Anda",
		"address" => "Alamat Anda",
		"hobby" => "Hobi Anda",
		"job" => "Pekerjaan Anda",
	],

	"thank" => "Terima kasih atas kontribusi anda.",
];
