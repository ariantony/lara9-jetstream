<?php

namespace App\Http\Controllers\Master;

use Inertia\Inertia;
use App\Models\Master\Agama;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $datum= Agama::paginate(10);
        return Inertia::render('Master/Agama/Index', [
            'datum' => Agama::query()
                ->when($request->input('search'), function ($query, $search){
                    $query->where('name', 'like', "%{$search}%");
                })
                ->paginate(10)
                ->withQueryString()
                ->through(fn($agama) => [
                    'id' => $agama->id,
                    'name' => $agama->name,
                    'description' => $agama->description
                ]),
            'filters' => $request->only(['search'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Master/Agama/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => ['required']
        ]);

        $data = [];
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['created_at'] = now();

        Agama::insert($data);

        return redirect()->route('agama.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Agama  $agama
     * @return \Illuminate\Http\Response
     */

    public function edit(Agama $agama)
    {
        return Inertia::render('Master/Agama/Edit',[
            'datum' => $agama,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agama $agama)
    {
        $agama->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return redirect()->route('agama.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Agama  $agama
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agama $agama)
    {
        $agama->delete();
        return redirect()->route('agama.index');
    }
}
