<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SpatieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datum = User::from('users')
        //     ->selectRaw('id')
        //     ->where('id','=',1)
        //     ->get();

        // # Membuat role dan permission dan sertakan Pula guard_namenya
        // Role::create(['guard_name' => 'web', 'name' => 'manager']);
        // Permission::create(['guard_name' => 'web', 'name' => 'edit articles2']);

        // # Assign Role To Permission
        //$role = Role::find(1);
        //$role->givePermissionTo(3,4);

        // # Assign User To Role
        //$user = User::find(1);
        // //$user->assignRole(3,4);
        // $user->assignRole('writer1');
        
        // # Get Permission via user and role
        //$datum = $user->getAllPermissions();
        //$datum = $user->getPermissionsViaRoles();
        //$datum = $user->getRoleNames();
        //$datum = $user->getPermissionNames();
        //$datum = $user->getDirectPermissions();
        $datum = User::with('permissions')->get();

        // $role->givePermissionTo($permission);
        return Inertia::render('Spatie', compact('datum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
