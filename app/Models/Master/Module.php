<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Module extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'sys_tmst_module';


    protected $fillable = [
        'id',
        'name',
        'table_name',
        'slug',
        'description',
        'created_date',
    ];

    protected $dates = ['deleted_at'];
}
