<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Agama extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'sys_tmst_agama';


    protected $fillable = [
        'id',
        'name',
        'description',
        'created_date',
    ];

    protected $dates = ['deleted_at'];
}
